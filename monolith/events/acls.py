import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1&page=1"
    response = requests.get(url, headers=headers)
    data = response.json()

    if data.get("photos"):
        return {"picture_url": data["photos"][0]["src"]["medium"]}
    return {"picture_url": None}


def get_weather_data(city, state):
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(geocode_url)
    geocode_data = response.json()

    if geocode_data:
        lat, lon = geocode_data[0]["lat"], geocode_data[0]["lon"]
        weather_url = f"http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
        response = requests.get(weather_url)
        weather_data = response.json()

        weather = {
            "temp": weather_data["main"]["temp"],
            "description": weather_data["weather"][0]["description"],
        }
    else:
        weather = None

    return weather
